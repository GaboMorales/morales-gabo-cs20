# Gabo Morales
# Computer Science 20
# Calculator for the Area of a Triangle

# Base Value (input)
value_of_base = input("Enter The Value Of The Base Here")
value_of_base = float(value_of_base)

# Height Value (input)
value_of_height = input('Enter The Value Of The Height Here')
value_of_height = float(value_of_height)

# Calculation
area_of_triangle = (value_of_base * value_of_height) / 2

# Output
print("Area of Triangle", area_of_triangle)
