# Gabo Morales
# Computer Science 20
# Calculator for the Area of a Trapezoid

# Height of the Trapezoid (Input)
height_of_trapezoid = input("Enter Value for the Height Here")
height_of_trapezoid = float(height_of_trapezoid)

# Length of Bottom Base (Input)
length_of_bottom_base = input("Enter Value for Length of the Bottom Base Here")
length_of_bottom_base = float(length_of_bottom_base)

# Length of the Top Base (Input)
length_of_the_top_base = input("Enter Value for the Length of the Top Base Here")
length_of_the_top_base = float(length_of_the_top_base)

# Area of Trapezoid Calculation
area_of_trapezoid = 0.5 * (length_of_bottom_base + length_of_the_top_base) * height_of_trapezoid

# Output
print("Area of Trapezoid", area_of_trapezoid)
