# Gabo Morales
# Computer Science 20
# Fahrenheit to Celsius Calculator

# Fahrenheit Input
fahrenheit_temperature = input("Enter Value Here")
fahrenheit_temperature = float(fahrenheit_temperature)

# Fahrenheit to Celsius Calculation
fahrenheit_to_celcius = (fahrenheit_temperature - 32) * (5/9)

# Output
print("Temperature in Celsius", fahrenheit_to_celcius)
