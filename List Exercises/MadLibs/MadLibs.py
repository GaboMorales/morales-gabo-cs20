#Mad Libs

#Change the following at your own risk

import random

#Fill list of adjectives

adjectiveList = []
file = open("adjectives.dat")
 
for line in file:
    line = line.strip()
    adjectiveList.append(line)

file.close()



#Fill list of nouns

nounList = []
file = open("nouns.dat")
 
for line in file:
    line = line.strip()
    nounList.append(line)

file.close()



#Fill list of verbs

verbList = []
file = open("verbs.dat")
 
for line in file:
    line = line.strip()
    verbList.append(line)

file.close()


#Start working below

#Exercsie 1
#Have the computer generate a MadLib of the form:
#The <noun> <verb> after the <adjective> <adjective> <noun> while the <noun> <verb> the <noun>
#Do not just print the MadLib - store it in a string!


#Exercise 2
#The code belowe writes a single line to a file.  Modify it so that it writes ten randomly generated MadLibs to the file.

file = open("madlibs.dat", "w")

content = "This is what will be written into the file."
file.write(content)

file.close()

#Exercise 3
#Create your own MadLib story using the same lists: