# Gabo Morales
# Computer Science
# Quiz Assingment

question_correct = 0
print("Quiz time!\n")

# Number Question
print("Solve: How many inches are in 13 inches + 5 feet?")
number_question = input("Enter your answer here: ")
number_question = float(number_question)
if number_question == 73:
    print("Correct!\n")
    question_correct += 1
else:
    print("Wrong\n")

# Text Question
print("Who wrote the book 'It'?")
text_question = input("Enter your answer here: ")
if text_question.lower() == "stephen king":
    print("Correct!\n")
    question_correct += 1
else:
    print("Wrong\n")

# Multiple Choice Question
print("How would you factor x + 11x + 18")
print("a. = (x+2)(x+9)")
print("b. = (x-2)(x+9)")
print("c. = I don't know...")
print("d. = It can't be factored")
multiple_choice = input("Enter your answer here: ")
if multiple_choice.lower() == "a":
    print("Correct!\n")
    question_correct += 1
else:
    print("Wrong\n")

# Number Question #2
print("Solve this: -2|-5-(-9)|+7")
number_question_2 = input("Enter your answer here: ")
number_question_2 = float(number_question_2)
if number_question_2 == -1:
    print("Correct!\n")
    question_correct += 1
else:
    print("Wrong\n")

# Multiple Choice Question #3
print("What is the formula for the volume of a rectangular prism?")
print("a. = (base * height)/2")
print("b. = length * width")
print("c. = length * width * height")
print("d. = (lenght * width)/height")
multiple_choice_2 = input("Enter your answer here: ")
if multiple_choice_2.lower() == "c":
    print("Correct!\n")
    question_correct += 1
else:
    print("Wrong\n")


# End of the Quiz
print("End of the Quiz. This is your score", question_correct,)
if question_correct == 5:
    print("Congratulations!, you got 100%!")
elif question_correct == 4:
    print("Great Job!, your score is 80%!")
elif question_correct == 3:
    print("Good, Your score is 60%")
elif question_correct == 2:
    print("Your score is 40%")
elif question_correct == 1:
    print("Your score is 20%")
elif question_correct == 0:
    print("Your score is 0%")
