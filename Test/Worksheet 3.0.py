import random
print("Welcome to time travel")
print()
print("It is the year 2050, you are a scientist and you live in a zombie apocalypse world.")
print("Due to your research you have found the cure of the zombie outbreak")
print("but there is a problem")
print("80% of the population is already dead")
print("and the only way to fix this is to use the time machine that you built,")
print("in order to prevent the outbreak from occurring.")
print("The outbreak started in 2019 in a science lab at Dr Martin Lebouldus high school.")
print("You have gone back to 2019, but you have some problems,")
print("the time machine can only travel back to time but is not that precise for location.")
print("You have landed at a forest and now you must get to the school which is 20 miles away.")
print()
print("GET TO THE SCHOOL BEFORE THE APOCALYPSE BEGINS!")
print()
print()
print("FACTORS TO KEEP IN MIND")
print()
print("You only have 240 minutes(4 hours) before the event occurs.")
print("You are 20 miles away from your objective.")
print("You only have 2 packs of water and hotdogs.")
print("Whenever you consume one of your hotdog and water from the bottle, your stamina will increase in 40 and you will "
      "lose 35 minutes ")
print("Whenever you rest, your stamina will regain to 35 and you will lose 65 minutes ")
print()

time = 240
distance = 20
your_stamina = 100
supplies = 2

done = False

while not done:
    print("A. Rest")
    print("B. Run to the objective")
    print("C. Walk to the objective")
    print("D. Use consumable")
    print("E. Status check")
    print("Q. Quit")
    print()
    answer = input("Enter your answer here: ")

# Random event
    bonus_event = random.randrange(16)
    if answer.lower() == "b" and "c" and bonus_event == 6:
        print("While you were in your way stop the outbreak from occurring, you came across with a farm. The owner of")
        print("the farm decided that he wants to help you in your mission, therefore, he cooked you a meal and now your")
        print("stamina increased by 60, not only that he also decided to give you 3 more consumables that you could use")
        print("in your mission. All this took you 10 minutes")
        print()
        your_stamina = + 60
        supplies += 3
        time -= 10
# If answer is a (rest)

    if answer.lower() == "a":
        print("You rested for 45 minutes")
        your_stamina += 36
        time -= 65
        if time <= 0:
            print("Oh no, you ran out of time, the zombie apocalypse has begun")
            done = True
        elif time <= 60 and time >= 31:
            print("Hurry up!, you only have less than 1 hour left")
        elif time <= 30 and time >= 1:
            print("You have around 30 minutes or less to prevent the outbreak!!!, ")
        elif your_stamina >= 51 <= 100:
            print("Your stamina is pretty decent right now")
            print()
        elif your_stamina >= 1 <= 34:
            print("You are getting very tired, your stamina is between 34 to 1."
                  "Regenerate your stamina before you die!")
            print()

    # If answer is b (run)

    if answer.lower() == "b":
        distance -= 2.5
        time -= 20
        your_stamina -= 20
        if distance <= 0 and time >= 1 and your_stamina >= 1:
            print("Congratulations!, you have saved the world by avoiding the zombie apocalypse")
            print("You have won the game")
            done = True
        elif time <= 0:
            print("Oh no, you ran out of time, the zombie apocalypse has begun")
            done = True
        elif time <= 60 and time >= 31:
            print("Hurry up!, you only have less than 1 hour left")
            print("You are", distance, "miles away from your objective")
        elif time <= 30 and time >= 1:
            print("You have around 30 minutes or less to prevent the outbreak!!!, "
                  "the destiny of the world in is your hands")
            print("You are", distance, "miles away from your objective")
        elif your_stamina <= 0:
            print("You died due to extreme fatigue from the lack of stamina")
            done = True
        elif your_stamina >= 51 <= 100:
            print("Your stamina is pretty decent right now")
            print("You are", distance, "miles away from your objective")
            print()
        elif your_stamina >= 1 <= 34:
            print("You are getting very tired, your stamina is between 34 to 1."
                  "Regenerate your stamina before you die!")
            print("You are", distance, "miles away from your objective")
            print()

    # If answer is c (walk)

    if answer.lower() == "c":
        distance -= 2
        your_stamina -= 10
        time -= 60
        if distance <= 0 and time >= 1 and your_stamina >= 1:
            print("Congratulations!, you have saved the world by avoiding the zombie apocalypse")
            print("You have won the game")
            done = True
        elif time <= 0:
            print("Oh no, you ran out of time, the zombie apocalypse has begun")
            done = True
        elif time <= 60 and time >= 31:
            print("Hurry up!, you only have less than 1 hour left")
            print("You are", distance, "miles away from your objective")
        elif time <= 30 and time >= 1:
            print("You have around 30 minutes or less to prevent the outbreak!!!, "
                  "the destiny of the world in is your hands")
            print("You are", distance, "miles away from your objective")
        elif your_stamina <= 0:
            print("You died due to extreme fatigue from the lack of stamina")
            done = True
        elif your_stamina >= 51 <= 100:
            print("Your stamina is pretty decent right now")
            print("You are", distance, "miles away from your objective")
            print()
        elif your_stamina >= 1 <= 34:
            print("You are getting very tired, your stamina is between 34 to 1."
                  "Regenerate your stamina before you die!")
            print("You are", distance, "miles away from your objective")
            print()

# If Answer is d (use consumable)

    if answer.lower() == "d":
        print("You have regained 45 stamina")
        print()
        your_stamina += 40
        time -= 35
        supplies -= 1
        if supplies == 0:
            print("Oh no, you ran out of supplies")

# If Answer is e (status check)

    if answer.lower() == "e":
        print("Your time remaining is:", time, "minutes")
        print("Your distance is:", distance, "miles")
        print("Your supplies remaining are:", supplies)
        print("The amount of stamina remaining is:", your_stamina)
        print()
        time -= 5
        print("You have lost 5 minutes while checking.")

# If Answer is q (quit)

    if answer.lower() == "q":
        done = True
        print()
        print("You have quit the game.")
    print()